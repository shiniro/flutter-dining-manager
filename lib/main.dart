/*
* Main
* Dependensies: Models - Pages 
* (Homepage, Authentification - FindUser)
* Configuration : theme (font, colors) - route
* AutoAuthentification if there is data in cookie
*/

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
// import 'package:flutter/rendering.dart'; //for debug 


import 'scoped-models/main.dart';
import 'pages/home.dart';
import 'pages/test.dart';
import 'pages/authentification/auth.dart';
import 'pages/authentification/find_user.dart';

void main() {
  // debugPaintSizeEnabled = true; //for debug 
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  bool _isAuthentificated = false;

  /********* Init State **********/
  @override
  void initState() {
    _model.autoAuthenticate();
    _model.userSubject.listen((bool isAuthentificated) {
      setState(() {
        _isAuthentificated = isAuthentificated;
      });
    });
    super.initState();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        theme: _buildAppTheme(),
        routes: {
          '/': (BuildContext context) => !_isAuthentificated ? AuthPage(_model) : HomePage(_model),
          '/test': (BuildContext context) => TestPage(),
        },
        onGenerateRoute: (RouteSettings settings) {
          final List<String> pathElements = settings.name.split('/');

          if (pathElements[0] != '') return null;

          if (!_isAuthentificated) { //only if not authentificarted
            if (pathElements[1] == 'find-user') { // find user pages
              final String findObject = pathElements[2]; // get what is after find-user/ in the url
              return MaterialPageRoute<bool>(
                builder: (BuildContext context) => 
                FindUserPage(findObject, _model),
              );
            }

            return MaterialPageRoute<bool>(
              builder: (BuildContext context) => AuthPage(_model)
            );
          }

          /* if (pathElements[1] == 'reservation') {
            final String reservationSeq = pathElements[2];
          } */

          return null;
        },
        onUnknownRoute: (RouteSettings settings) {
          return MaterialPageRoute(
            builder: (BuildContext context) => HomePage(_model)
          );
        },
      ),
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * Build Theme
  */
  ThemeData _buildAppTheme() {
    return ThemeData(
      accentColor: Color(0xff00b9f2),
      primaryColor: Color(0xff00b9f2),
      buttonColor: Color(0xff00b9f2),
      appBarTheme: AppBarTheme(color: Color(0xff333333)),
      fontFamily: 'NoteSans',
      textTheme: TextTheme(
        headline: TextStyle(fontWeight: FontWeight.w500),
        title: TextStyle(fontWeight: FontWeight.w500),
        body1: TextStyle(fontSize: 14.0, color: Color(0xff333333)),
      ),
    );
  }
}