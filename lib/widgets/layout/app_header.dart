/*
* App Header
* Build the App header
* Parameters :
* - required: Title
* - optional: actions (right side of the title)
*/

import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final List<Widget> actions;

  AppHeader(this.title, [this.actions]);

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      title: Center(
        child: Text(
          title,
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w500,
            fontFamily: 'NoteSans',
            color: Colors.white
          ),
        )
      ),
      backgroundColor: Theme.of(context).appBarTheme.color,
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      actions: actions,
    );
  }
}