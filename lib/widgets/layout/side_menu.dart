/*
* Side Menu
* Dependensies: Models
* Build the App Side Menu
*/
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';

class SideMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SideMenuState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class SideMenuState extends State<SideMenu> {
  final List<Map<String, String>> _menuLinks  = [
    {
      'title': '예약 관리',
      'link': '/'
    },
    {
      'title': '고객관리',
      'link': '/test'
    },
    {
      'title': '분석리포트',
      'link': '/'
    },
    {
      'title': '문자',
      'link': '/'
    },
    {
      'title': '테이블 단가입력',
      'link': '/'
    },
    {
      'title': '설정',
      'link': '/'
    }
  ];

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
          _buildDrawerHeader(),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _menuLinks.length,
            itemBuilder: (context, index) {
              return Column(
                children: <Widget>[
                  _buildMenuListTile(context, _menuLinks[index]['title'], _menuLinks[index]['link']),
                  Divider(
                    height: 1.0,
                  ),
                ],
              );
            },
          )
        ]
      )
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * Build Side Menu Title
  */
  Widget _buildSideMenuTitle(title) {
    return Center(
      child: Text(
        title,
        style: TextStyle(
          fontSize: 23.0,
          fontWeight: FontWeight.w500
        )
      )
    );
  }



  /*
  * Build Drawer Header
  */
  Widget _buildDrawerHeader() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Container(
          height: 270.0,
          child: DrawerHeader(
            margin: EdgeInsets.all(0.0),
            padding: EdgeInsets.all(0.0),
            child: Stack(
              children: <Widget> [
                model.user.planCode == 'FR'
                ? Container()
                : _buildPlanIcon(model.user.planCode),
                Container (
                  padding: EdgeInsets.symmetric(vertical: 88.0),
                  child: Column (children: <Widget>[
                    model.user.restaurantName != null ? _buildSideMenuTitle(model.user.restaurantName) : Container(),
                    _buildSideMenuTitle(model.user.storeName)
                  ],)
                ),
                _buildChangeStoreButton(),
              ]
            )
          )
        );
      },
    );
  }



  /*
  * Build Change Store Button
  */
  Widget _buildChangeStoreButton() {
    return Positioned(
      top: 190,
      left: 0,
      right: 0,
      child: Container(
        width: 100.0,
        child: Center(
          child:OutlineButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            borderSide: BorderSide(
              color: Theme.of(context).accentColor, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: .7, //width of the border
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.7, vertical: 9.3),
            child: Text(
              '다른 매장 선택',
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: 12.0
              ),
            ),
            onPressed: () {
            },
          )
        )
      ),
    );
  }



  /*
  * Build Menu List Title
  */
  Widget _buildMenuListTile(BuildContext context, String title, String link) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(
          color: Colors.black,
          fontSize: 15.0,
        )
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 24.7, vertical: 0),
      onTap: () {
        bool isNewRouteSameAsCurrent = false;

        // Check if same route
        Navigator.popUntil(context, (route) {
        if (route.settings.name == link) {
          isNewRouteSameAsCurrent = true;
        }
          return true;
        });

        if (!isNewRouteSameAsCurrent) {
          Navigator.pushReplacementNamed(context, link);
        } else {
          Navigator.pop(context);
        }
      }
    );
    // Divider();
  }



  /*
  * Build Plan Icon
  */
  Widget _buildPlanIcon(plan) {
    String imagePath;
    double width, height;

    switch(plan) {
      case 'BA':
        imagePath = 'assets/img/icons/icon_basic.png';
        width = 41.7;
        height = 26.0;
        break;

      case 'PL':
        imagePath = 'assets/img/icons/icon_plus.png';
        width = 43.0;
        height = 30.3;
        break;

      case 'PR':
        imagePath = 'assets/img/icons/icon_premium.png';
        width = 76.7;
        height = 26.0;
        break;
    }

    return Positioned(
      top: 19.7,
      left: 0,
      right: 0,
      child: Image.asset(
        imagePath,
        width: width,
        height: height,
      )
    );
  }
}