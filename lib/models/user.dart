/*
* User Model
* class User: the user that login the account - get information after login
*/

import 'package:flutter/material.dart';

class User {
  final String phone;
  final String loginId;
  final String password;
  final String token; // get in the request header
  final String restaurantName;
  final String storeName;
  final String planCode;
  final bool hasSwitchStore;

  User({
    @required this.phone,
    @required this.loginId,
    @required this.password,
    @required this.token,
    @required this.restaurantName,
    @required this.storeName,
    @required this.planCode,
    @required this.hasSwitchStore
  });
}