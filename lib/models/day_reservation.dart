/*
* DayReservation Model
* class DayReservation: Number of reservation of the day (for the calendar)
*/

import 'package:flutter/material.dart';

class DayReservation {
  final DateTime reservationDate;
  final int dawn;
  final int dinner;
  final int guests;
  final int lunch;
  final int waiting;
  final String name; // check name for holidays

  DayReservation({
    @required this.reservationDate,
    @required this.dawn,
    @required this.dinner,
    @required this.guests,
    @required this.lunch,
    @required this.waiting,
    @required this.name
  });
}