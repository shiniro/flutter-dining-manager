/*
* HomePage (calendar)
* Dependensies: Main Model - Global Layout - Day Reservation
* (Homepage, Authentification - FindUser)
* Configuration : theme (font, colors) - route
* AutoAuthentification if there is data in cookie
*/
import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; // for DateFormat
import 'package:scoped_model/scoped_model.dart';

import '../models/day_reservation.dart';
import '../scoped-models/main.dart';
import '../widgets/layout/global_layout.dart';

class HomePage extends StatefulWidget {
  final MainModel model;

  HomePage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _HomePageState extends State<HomePage> {
  final _today = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0);
  DateTime _currentDate =DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0);
  final PageController _homePageController = PageController(initialPage: 4242);

  /********* Init State **********/
  @override
  void initState() {
    // widget.model.logout();
    setState(() {
      _currentDate = widget.model.currentDate;
    });
    super.initState();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    final double _deviceWidth = MediaQuery.of(context).size.width;
    final double _targetWidth = _deviceWidth > 550.0 ? 500.0 : _deviceWidth;

    return GlobalLayout(
      title: widget.model.user.restaurantName != null ? '${widget.model.user.restaurantName}  ${widget.model.user.storeName} 예약 현황' : '${widget.model.user.storeName} 예약 현황',
      actions: _buildAppHeaderActions(),
      body: Container(
        child: Column(children: <Widget>[
          _buildMonthDateTitle(_targetWidth),
          _buildViewPage(),
          _buildReservationButton(_targetWidth)
        ],) 
      ),
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * APP HEADER ACTIONS
  * build icon (notification)
  */
  List<Widget> _buildAppHeaderActions() {
    return [
      ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return IconButton(
            icon: Image.asset(
              'assets/img/icons/icon_notification.png',
              width: 18.7,
              height: 20.7,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/test');
            },
          );
        }
      )
    ];
  }



  /*
  * MONTH DATE TITLE
  * Year.Month
  */
  Widget _buildMonthDateTitle(double targetWidth) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.085,
      margin: EdgeInsets.all(0.0),
      width: targetWidth,
      color: Color(0xff333333),
      padding: EdgeInsets.symmetric(vertical: 9.7),
      child: Center(
        child: Text(
          widget.model.currentDate.month < 10 ? '${_currentDate.year}.0${_currentDate.month}' : '${_currentDate.year}.${_currentDate.month}',
          style: TextStyle(
            fontSize: 24.0, 
            fontWeight: FontWeight.w500,
            color: Color(0xffab52eb),
          )
        )
      ),
    );
  }



  /*
  * BUILD VIEWPAGE
  * Build viewpage (slider)
  */
  Widget _buildViewPage() {
    return Expanded (
      child: PageView.builder(
        controller: _homePageController,
        scrollDirection: Axis.horizontal,
        onPageChanged: (int position) {
          setState(() {
            _currentDate = DateTime(_today.year, _today.month  + position - 4242, _today.day);
          });
          widget.model.changeCurrentDate(DateTime(_today.year, _today.month  + position - 4242, _today.day));
        },
        itemBuilder: (context, position) {
          return _buildCalendar();
        },
      ),
    );
  }



  /*
  * BUILD CALENDAR
  * 
  */
  Widget _buildCalendar() {
    // variable for grid height
    final double _itemHeight = (MediaQuery.of(context).size.height - kToolbarHeight - ((MediaQuery.of(context).size.height * 0.121) * 2)) / 5;
    final double _itemWidth = MediaQuery.of(context).size.width / 7;

    // get month reservations : null or Map
    final monthReservtations = widget.model.allDayReservations['${_currentDate.year}${_currentDate.month}'];

    // if haven't fetch data for the month yet => fetch data
    if(monthReservtations == null || (monthReservtations != null && monthReservtations['0'] == null)) {
      widget.model.fetchDayReservations(_currentDate);
    }

    // build
    return Container(
      child: Column(
        children: <Widget>[
          _buildWeekDaysTable(),
          ScopedModelDescendant<MainModel>(
            builder: (BuildContext context, Widget child, MainModel model) {
              return Expanded(
                child: GridView.count(
                  primary: true,
                  shrinkWrap: true,
                  crossAxisCount: 7,
                  childAspectRatio: (_itemWidth / _itemHeight),
                  children: _buildCalendarMonth(model),
                )
              );
            }
          )
        ],
      )
    );
  }



  /*
  * BUILD WEEK DAYS TABLE
  * BUG FLUTTER : Table with more than 6 elements => infinite loop on iosSimulator
  */
  Widget _buildWeekDaysTable() {

    // final List<String> _daysOfWeeks = ['일','월','화','목','금','토'];
    final List<String> _daysOfWeeks = ['일','월','화','수','목','금','토'];

    return Container(
      color: Color(0xff333333),
      child: Row(
        children: _daysOfWeeks.map((String day) {
            return _buildWeekDay(day);
          }
        ).toList()
      ),
    );
    /* return Table (
      border: TableBorder.all(color: Color(0xff333333)),
      children: <TableRow>[
        TableRow(
          children: _daysOfWeeks.map((String day) {
            return _buildWeekDay(day);
          }
        ).toList()) 
      ],
    );  */
  }

  /*
  * BUILD WEEK DAY
  */
  Widget _buildWeekDay(day) {
    final double _itemWidth = MediaQuery.of(context).size.width / 7;
    return /* TableCell(
      child: */ Container(
        width: _itemWidth,
        decoration: BoxDecoration(
          color: Color(0xff333333),
          border: Border.all(
            width: 1.0,
            color: Color(0xff333333)
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 7.3),
        child: Text(
          day,
          style: TextStyle(
            fontSize: 11.0,
            color: day == '일' ? Color(0xffff4c4c) : (day == '토' ? Color(0xff5c8bbf) : Color(0xff808080))
          )
        )
      //),
    );
  }



  /*
  * BUILD CALENDAR MONTH
  */
  List<Widget> _buildCalendarMonth(MainModel model) {
    List<DateTime> calendarDates = [];

    final int _monthDays = DateTime(_currentDate.year, _currentDate.month + 1, 0).day;
    final int _firstDayMonth = dayOfWeekNumber(DateTime(_currentDate.year, _currentDate.month, 1));
    final int _lastDayMonth = dayOfWeekNumber(DateTime(_currentDate.year, _currentDate.month, _monthDays));

    //Add Days in CalendarDates List
    // previous month days
    if (_firstDayMonth > 0) {
      final int prevMonthDays = DateTime(_currentDate.year, _currentDate.month - 2, 0).day;
      for(int i = _firstDayMonth; i > 0; i--) {
        calendarDates.add(DateTime(_currentDate.year, _currentDate.month - 1, prevMonthDays - i));
      }
    }
    //current month
    for(int i = 1; i <= _monthDays; i++) {
      calendarDates.add(DateTime(_currentDate.year, _currentDate.month, i));
    }
    // next month days
    if (_lastDayMonth < 6) {
      for(int i = 1; i <= 6 - _lastDayMonth; i++) {
        calendarDates.add(DateTime(_currentDate.year, _currentDate.month + 1, i));
      }
    }

    // build
    return calendarDates.map((DateTime date) {
        return _buildCalendarCell(date, model);
      }).toList();
  }
  


  /*
  * BUILD CALENDAR CELL
  */
  Widget _buildCalendarCell(DateTime date, MainModel model) {
    Color dayColor;

    DayReservation _dayReservation = null;
    
    // if the day have a reservation
    if (model.allDayReservations['${date.year}${date.month}'] != null && model.allDayReservations['${date.year}${date.month}']['${date.day}'] != null) {
      _dayReservation = model.allDayReservations['${date.year}${date.month}']['${date.day}'];
    }

    // color of the day
    if(_currentDate.month != date.month) {
      dayColor = Color(0xffcccccc);
    } else {
      if(_dayReservation != null && _dayReservation.name != null) dayColor = Color(0xffff4c4c);
      else if(dayOfWeekNumber(date) == 0) dayColor = Color(0xffff4c4c);
      else if(dayOfWeekNumber(date) == 6) dayColor = Color(0xff5c8bbf);
      else dayColor = Color(0xff333333);
    }
    
    // build
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7.3, vertical: 5),
      decoration: new BoxDecoration(
        border: new Border.all(
          width: date == _today ? 1 : .3,
          color: date == _today ? Color(0xffab52eb) : Color(0xffe6e6e6)
        )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // day
          Text(
            date.day.toString(),
            style: TextStyle(
              color: dayColor
            )
          ),
          // dawn if baseDate == D
          model.baseDate == 'D' ? 
            _dayReservation != null && _dayReservation.dawn != 0 ?
            _buildReservationDataText(date, 'dawn', _dayReservation.dawn) 
            : Container()
          : Container(),
          // lunch
          _dayReservation != null && _dayReservation.lunch != 0
          ? _buildReservationDataText(date, 'lunch', _dayReservation.lunch) : Container(),
          // dinner
          _dayReservation != null && _dayReservation.dinner != 0
          ? _buildReservationDataText(date, 'dinner', _dayReservation.dinner) : Container(),
          // dawn if baseDate == Y
          model.baseDate == 'Y' ? 
            _dayReservation != null && _dayReservation.dawn != 0 ?
            _buildReservationDataText(date, 'dawn', _dayReservation.dawn) 
            : Container()
          : Container(),
          // waiting
          _dayReservation != null && _dayReservation.waiting != 0
          ? _buildReservationDataText(date, 'waiting', _dayReservation.waiting) : Container(),
          // guest
          _dayReservation != null && _dayReservation.guests != 0
          ? _buildReservationDataText(date, 'guests', _dayReservation.guests) : Container(),
        ]
      )
    );
  }



  /*
  * BUILD RESERVATION DATA TEXT
  * build text for reservation data (dawn, lunch, dinne, waiting, guest)
  */ 
  Widget _buildReservationDataText(DateTime date, String type, int numberOfReservation) {
    String _text;
    Color _colorText;
    
    switch (type) {
      case 'dawn':
        _text = '새벽';
        _colorText = Color(0xffab52eb);
        break;

      case 'lunch':
        _text = '점심';
        _colorText = Color(0xffff910c);
        break;

      case 'dinner':
        _text = '저녁';
        _colorText = Color(0xff08b89d);
        break;

      case 'waiting':
        _text = '대기';
        _colorText = Color(0xff00b9f2);
        break;

      case 'guests':
        _text = '인원';
        _colorText = Color(0xff808080);
        break;
    }

    if(date.isBefore(_today)) {
      _colorText = Color(0xffcccccc);
    }

    // build
    return Text(
      '${_text} ${numberOfReservation}',
      style: TextStyle(
        color: _colorText,
        fontSize: MediaQuery.of(context).size.width < 700 ? 7 : 9.3
      )
    );
  }



  /*
  * BUILD RESERVATION BUTTON
  */
  Widget _buildReservationButton(double targetWidth) {
    return Container(
      width: targetWidth,
      child: RaisedButton(
        elevation: 0.0,
        padding: EdgeInsets.symmetric(vertical: 16.5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '예약 등록 하러 가기',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w500 
              )
            ),
            SizedBox(
              width: 13.7,
            ),
            Icon(
              Icons.arrow_forward,
              color: Colors.white,
            ),
          ],
        ),
        onPressed: () {

        },)
    );
  }

  /******************************/
  /********** METHODS ***********/ 
  /******************************/

  /*
  * DAY OF THE WEEK :
  * return date in number (sunday = 0) and -1 if can't find
  */
  int dayOfWeekNumber(DateTime date) {
    final _day = DateFormat("E").format(date);
    switch(_day) {
      case 'Sun':
      case '일':
        return 0;
        break;
      case 'Mon':
      case '월':
        return 1;
        break;
      case 'Tue':
      case '화':
        return 2;
        break;
      case 'Wed':
      case '수':
        return 3;
        break;
      case 'Thu':
      case '목':
        return 4;
        break;
      case 'Fri':
      case '금':
        return 5;
        break;
      case 'Sat':
      case '토':
        return 6;
        break;
    };
    return -1;
  }
}