import 'package:flutter/material.dart';

class TestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 200.0),
        child: Table(
        children: <TableRow>[
          TableRow(
            children: <TableCell>[
              TableCell(
                child: Text('test1'),
              ),
              TableCell(
                child: Text('test2'),
              ),
              TableCell(
                child: Text('test3'),
              ),
              TableCell(
                child: Text('test4'),
              ),
              TableCell(
                child: Text('test5'),
              ),
              TableCell(
                child: Text('test6'),
              ),
              /* TableCell(
                child: Text('test7'),
              ) */
            ]
          )
        ],
      )
      )
    );
  }
}