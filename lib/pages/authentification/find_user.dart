/*
* Find User - id or password - send email page
* depend on url show id or password form, if request success show send email page
* Dependensies: Models - Global Layout
*/

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main.dart';

import '../../widgets/layout/global_layout.dart';

class FindUserPage extends StatefulWidget {
  final String findObject;
  final MainModel model;

  FindUserPage(this.findObject, this.model);

  @override
  State<StatefulWidget> createState() {
    return _FindUserPageState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _FindUserPageState extends State<FindUserPage> {
  bool _successSendMail = false;
  bool _isDisabled = true;

  final Map<String, dynamic> _findIdFormData = {
    'storeName': null,
    'phone': null
  };

  String _email = null;
  bool _autovalidate = false;

  final TextEditingController _phoneTextFormFieldController = TextEditingController();
  final TextEditingController _storeNameTextFormFieldController = TextEditingController();
  final TextEditingController _emailTextFormFieldController = TextEditingController();

  final GlobalKey<FormState> _findUserFormKey = GlobalKey<FormState>();

  /********* Init State **********/
  @override
  void initState() {
    _phoneTextFormFieldController.addListener(() => toogleDisabledButton());
    _storeNameTextFormFieldController.addListener(() => toogleDisabledButton());
    _emailTextFormFieldController.addListener(() => toogleDisabledButton());
    super.initState();
  }

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: '아이디 찾기',
      showMenu: false,
      singleChildScroll: true,
      horizontalPadding: 30.0,
      verticalPadding: 20.0,
      actions: <Widget>[
        _successSendMail ? Container() : _buildHeaderButton()
      ],
      body: _successSendMail ? _buildSendEmail() : _buildFindUserForm(),
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * BUILD HEADER BUTTON (submit button)
  * onPress => submitForm function
  */
  Widget _buildHeaderButton() {
    return ButtonTheme(
      height: 10.0,
      child: FlatButton(
        disabledTextColor: Color(0xff808080),
        textColor: Colors.white,
        child: Container(
          height: 34.0,
          padding: EdgeInsets.symmetric(vertical: 5.3, horizontal: 20.5),
          decoration: BoxDecoration(
            color: _isDisabled ? Color(0xff333333) : Color(0xffff910c),
            border: Border.all(color: Color(0xff808080)),
            borderRadius: BorderRadius.circular(25.0),
          ),
          child: Text('확인'),
        ),
        onPressed: (!_isDisabled) ? () =>  _submitForm(widget.model.findId, widget.model.findPassword) : null,
      )
    );
  }



  /*
  * BUILD SEND EMAIL PAGE
  */
  Widget _buildSendEmail() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 30.0),
          // icon
          Center(
            child: Image.asset(
              'assets/img/icons/icon_mailing.png',
              width: 75.0,
              height: 30.0,
            ),
          ),
          SizedBox(height: 50.0),
          // Text with email
          Center(
            child: RichText(
              text: TextSpan(
                style: TextStyle(
                  color: Theme.of(context).textTheme.body1.color,
                  fontWeight: FontWeight.w500
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: _email,
                    style: TextStyle(
                      color: Theme.of(context).accentColor
                    ),
                  ),
                  TextSpan(
                    text: ' 으로',
                  )
                ]
              ),
            ),
          ),
          SizedBox(height: 14.0),
          // second line of text
          Center(
            child: Text(
              '로그인 아이디가 발송 되었습니다.',
              style: TextStyle (fontWeight: FontWeight.w500)
            )
          )
        ],
      )
    );
  }



  /*
  * BUILD FIND USER FORM
  * check url : if id or password => display right form
  */ 
  Widget _buildFindUserForm() {
    return Form(
      key: _findUserFormKey,
      autovalidate: _autovalidate,
      child: widget.findObject == 'id' ? _buildFindIdContent() : _buildFindPasswordContent(),
    );
  }

  /*
  * BUILD FIND ID CONTENT
  */
  Widget _buildFindIdContent() {
    return Column (
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          '식당 이름을 입력해주세요.',
          style: TextStyle(fontWeight: FontWeight.w500)
        ),
        SizedBox(height: 18.0),
        _buildStoreNameTextField(),
        SizedBox(height: 18.0),
        Text(
          '식당 전화번호를 입력해주세요.',
          style: TextStyle(fontWeight: FontWeight.w500)
        ),
        SizedBox(height: 18.0),
        _buildPhoneTextField()
      ],
    );
  }



  /*
  * BUILD FIND PASSWORD CONTENT
  */
  Widget _buildFindPasswordContent() {
    return Column (
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          '가입하신 이메일 주소를 써 주세요.',
          style: TextStyle(fontWeight: FontWeight.w500)
        ),
        SizedBox(height: 18.0),
        _buildEmailTextField(),
      ],
    );
  }



  /*
  * BUILD PHONE TEXT FIELD
  */
  Widget _buildPhoneTextField() {
    return TextFormField(
      controller: _phoneTextFormFieldController,
      decoration: _buildInputDecoration('예) 0232882011'),
      keyboardType: TextInputType.phone,
      validator: (String value) {
        if (value.isEmpty || !RegExp(r"^(?:(?:\+|00)82|0)(\s*[1-9]|\s*[0-9][0-9])(?:[\s.-]*\d{4}|[\s.-]*\d{3})(?:[\s.-]*\d{4})$").hasMatch(value)) {
          return _autovalidate ? '전화번호가 없습니다.' : '';
        }
      },
      onEditingComplete: toogleDisabledButton,
      onSaved: (String value) {
        _findIdFormData['phone'] = value;
      },
    );
  }



  /*
  * BUILD STORE NAME TEXT FIELD
  */
  Widget _buildStoreNameTextField() {
    return TextFormField(
      controller: _storeNameTextFormFieldController,
      decoration: _buildInputDecoration('예) 다이닝매니저'),
      validator: (String value) {
        if (value.isEmpty || value.length < 3) {
          return _autovalidate ? '식당 이름이 없습니다.' : '';
        }
      },
      onEditingComplete: toogleDisabledButton,
      onSaved: (String value) {
        _findIdFormData['storeName'] = value;
      },
    );
  }

  /*
  * BUILD EMAIL TEXT FIELD
  */
  Widget _buildEmailTextField() {
    return TextFormField(
      controller: _emailTextFormFieldController,
      decoration: _buildInputDecoration('예) cs@diningmanger.co.kr'),
      validator: (String value) {
        if (value.isEmpty || !RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$").hasMatch(value)) {
          return _autovalidate ? '가입하신 이메일에서 임시 비밀번호를 확인하세요' : '';
        }
      },
      onEditingComplete: toogleDisabledButton,
      onSaved: (String value) {
        setState(() {
          _email = value;
        });
      },
    );
  }

  /******** UI Element *********/

  /*
  * INPUT DECORATION
  */
  InputDecoration _buildInputDecoration(placeholder) {
    return InputDecoration(
      hintText: placeholder,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20.7, vertical: 16.3),
      hintStyle: TextStyle(
        color: Color(0xffcccccc),
        fontWeight: FontWeight.w500
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
    );
  }

  /******************************/
  /********** METHODS ***********/ 
  /******************************/

  /*
  * SUBMIT FORM
  */
  void _submitForm(Function findId, Function findPassword) async {
    // if input empty or not correct => show error message and stop function
    if(!_findUserFormKey.currentState.validate()) {
      setState(() {
        _autovalidate = true;
      });
      return;
    }

    // saveData in variables
    _findUserFormKey.currentState.save();

    // http resquest
    Map<String, dynamic> userInformation;
    if (widget.findObject == 'id') {
      userInformation = await findId(_findIdFormData['storeName'], _findIdFormData['phone']);
    } else {
      userInformation = await findPassword(_email);
    }

    // handle response
    //success
    if (userInformation['success']) {
      setState(() {
        _successSendMail = true;
      });
    } else {
      //show error in a dialog box
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('An error Occured !'),
            content: Text(userInformation['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
      );
    }
  }



  /*
  * TOOGLE DISABLED BUTTON
  */
  void toogleDisabledButton() {
    if(!_findUserFormKey.currentState.validate()) {
      setState(() {
        _isDisabled = true;
      });
    } else {
      setState(() {
        _isDisabled = false;
      });
    }
  }
}