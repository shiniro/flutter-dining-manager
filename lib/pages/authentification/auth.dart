/*
* Login / Authentification page
* Dependensies: Models - Global Layout
*/

import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart'; // get device info
import 'package:uuid/uuid.dart'; //generate uuid for simulated divice
import 'package:url_launcher/url_launcher.dart'; // for outside links

import '../../scoped-models/main.dart';

import '../../widgets/layout/global_layout.dart';

class AuthPage extends StatefulWidget {
  final MainModel model;

  AuthPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

/******************************/
/*********** STATE ************/ 
/******************************/

class _AuthPageState extends State<AuthPage> {
  final Map<String, dynamic> _loginFormData = {
    'phone': null,
    'loginId': null,
    'password': null
  };
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  /********* Build **********/
  @override
  Widget build(BuildContext context) {
    return GlobalLayout(
      title: '로그인',
      showMenu: false,
      singleChildScroll: true,
      horizontalPadding: 30.0,
      verticalPadding: 20.0,
      body: Form(
        key: _loginFormKey,
        child: Column (
          children: <Widget>[
            _buildPhoneTextField(),
            SizedBox(height: 5.0),
            _buildLoginIdTextField(),
            SizedBox(height: 5.0),
            _buildPasswordTextField(),
            SizedBox(height: 30.0),
            _buildLoginButton(),
            SizedBox(height: 28.0),
            _buildLink({
              'iconUrl': 'assets/img/icons/icon_info_b_s.png',
              'iconWidth': 6.0,
              'iconHeight' : 12.0,
              'linkText': '서비스 안내 및 문의하기',
              'linkType': 'link',
              'url': 'http://www.diningmanager.co.kr/'
            }),
            SizedBox(height: 24.0),
            _buildLink({
              'iconUrl': 'assets/img/icons/icon_man_b_s.png',
              'iconWidth': 8.7,
              'iconHeight' : 10.7,
              'linkText': '회원가입',
              'linkType': 'link',
              'url': 'http://www.diningmanager.co.kr/signup_form.html'
            }),
            SizedBox(height: 24.0),
            _buildLink({
              'iconUrl': 'assets/img/icons/icon_search_b_s.png',
              'iconWidth': 10.3,
              'iconHeight' : 10.7,
              'linkText': 'ID 찾기',
              'linkType': 'redirect',
              'url': '/find-user/id'
            }),
            SizedBox(height: 24.0),
            _buildLink({
              'iconUrl': 'assets/img/icons/icon_lock_b_s.png',
              'iconWidth': 10.0,
              'iconHeight' : 13.3,
              'linkText': '비밀번호 찾기',
              'linkType': 'redirect',
              'url': '/find-user/password'
            }),
          ],
        ),
      ),
    );
  }

  /******************************/
  /********** WIDGETS ***********/ 
  /******************************/

  /*
  * PHONE TEXT FIELD
  */
  Widget _buildPhoneTextField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: '식당 전화번호',
        contentPadding: const EdgeInsets.symmetric(horizontal: 20.7, vertical: 16.3),
        hintStyle: TextStyle(
          color: Color(0xffcccccc),
          fontWeight: FontWeight.w500
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
      ),
      keyboardType: TextInputType.phone,
      validator: (String value) {
        if (value.isEmpty || !RegExp(r"^(?:(?:\+|00)82|0)(\s*[1-9]|\s*[0-9][0-9])(?:[\s.-]*\d{4}|[\s.-]*\d{3})(?:[\s.-]*\d{4})$").hasMatch(value)) {
          return '전화번호디가 틀렸습니다. 다시 확인해 주세요';
        }
      },
      onSaved: (String value) {
        _loginFormData['phone'] = value;
      },
    );
  }



  /*
  * LOGIN ID TEXT FIELD
  */
  Widget _buildLoginIdTextField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: '로그인 아이디',
        contentPadding: const EdgeInsets.symmetric(horizontal: 20.7, vertical: 16.3),
        hintStyle: TextStyle(
          color: Color(0xffcccccc),
          fontWeight: FontWeight.w500
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
      ),
      validator: (String value) {
        if (value.isEmpty || value.length < 3) {
          return '식당 아이디가 틀렸습니다. 다시 확인해 주세요.';
        }
      },
      onSaved: (String value) {
        _loginFormData['loginId'] = value;
      },
    );
  }



  /*
  * PASSWORD TEXT FIELD
  */
  Widget _buildPasswordTextField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        hintText: '비밀번호',
        contentPadding: const EdgeInsets.symmetric(horizontal: 20.7, vertical: 16.3),
        hintStyle: TextStyle(
          color: Color(0xffcccccc),
          fontWeight: FontWeight.w500
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xffcccccc), width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return '비밀번호디가 틀렸습니다. 다시 확인해 주세요.';
        }
      },
      onSaved: (String value) {
        _loginFormData['password'] = value;
      },
    );
  }



  /*
  * BUILD LOGIN BUTTON
  */
  Widget _buildLoginButton() {
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 16.3),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        color: Theme.of(context).primaryColor,
        textColor: Colors.white,
        child: Text(
          '로그인',
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontWeight: FontWeight.w500,
          )
        ),
        onPressed: () => _submitForm(widget.model.authenticate),
      )
    );
  }



  /*
  * LINKS
  */
  Widget _buildLink(Map<String, dynamic> linkInfo) {
    return InkWell(
      child: Row(
        children: <Widget>[
          Image.asset(
            linkInfo['iconUrl'],
            width: linkInfo['iconWidth'],
            height: linkInfo['iconHeight'],
          ),
          SizedBox(width: 16.0),
          Text(
            linkInfo['linkText'],
            style: TextStyle (
              color: Theme.of(context).accentColor,
              fontSize: 12.0,
              fontWeight: FontWeight.w500
            ),
          ),
        ],
      ),
      onTap: () {
        if (linkInfo['linkType'] == 'link') {
          _launchURL(linkInfo['url']);
        } else {
          _redirect(linkInfo['url']);
        }
      },
    );
  }

  /******************************/
  /********** METHODS ***********/ 
  /******************************/

  /*
  * GET PUSHID
  */
  Future<String> _getId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    //ios
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      // simulator -> geenrate a id
      if(!iosDeviceInfo.isPhysicalDevice) {
        return Uuid().v4();
      }
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else { // android
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      // simulator -> geenrate a id
      if(androidDeviceInfo.isPhysicalDevice) {
        return Uuid().v4();
      }
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }



  /*
  * SUBMIT FORM
  */
  void _submitForm(Function authenticate,) async {
    // if input empty or not correct => show error message and stop function
    if(!_loginFormKey.currentState.validate()) {
      return;
    }

    // saveData in variables
    _loginFormKey.currentState.save();

    final String pushId = await _getId();

    if (pushId.length > 0) {
      //http request
      Map<String, dynamic> userInformation = await authenticate(_loginFormData['phone'], _loginFormData['loginId'], _loginFormData['password'], pushId);
      
      //handle response
      if (userInformation['success']) {
        print(userInformation);
      } else {
         //show error in a dialog box
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('An error Occured !'),
              content: Text(userInformation['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      }
    }
  }



  /*
  * REDIRECT
  * Redirect to another app page
  */
  void _redirect(String url) {
    Navigator.pushNamed(context, url);
  }



  /*
  * LAUNCH URL
  * open another url page
  */
  void _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}