/*
* Connect Data Model
* Connect Data Models
* Methods : 
* - get isLoading, get baseDate, get currentDate
* - changeCurrentDate
*/

import 'package:scoped_model/scoped_model.dart';

const bool isProduction = bool.fromEnvironment('dart.vm.product');

class ConnectedDataModel extends Model {
  bool isLoadingData = false;
  String baseDateData = 'D';
  DateTime currentDateData = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0);
  final String apiUrl =  isProduction ? 'https://api.diningmanager.co.kr' : 'http://dev.diningmanager.co.kr';
  final Map<String, String> headers = {
    'Accept': 'application/json; text/plain',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
  };
}

class UtilityModel extends ConnectedDataModel {
  /************ Getters *************/
  bool get isLoading {
    return isLoadingData;
  }

  String get baseDate {
    return baseDateData;
  }

  DateTime get currentDate {
    return currentDateData;
  }

  /************ Methods *************/
  void changeCurrentDate(DateTime date) {
    currentDateData = date;
    notifyListeners();
  }
}