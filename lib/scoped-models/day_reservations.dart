/*
* Day Reservation Scoped Model (number of reservation for a day for calendar)
* Dependensies: DayReservation model - Connected Data Scoped Model
* Methods : 
* - get allDayReservations, get selectedDayReservation (depend on currentDate)
* - fetchDayReservations
* - addToDayReservations
*/

import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

import '../models/day_reservation.dart';
import './connected_data.dart';

class DayReservationModel extends ConnectedDataModel{
  final Map<String, Map<String,dynamic>> _dayReservations = Map();
  
  /************ Getters *************/
  /*
  * Get All DayReservation
  */
  Map<String, Map<String,dynamic>> get allDayReservations {
    return Map.from(_dayReservations);
  }

  /*
  * Get selected DatReservation (depend on currenDate)
  */
  DayReservation get selectedDayReservation {
    return _dayReservations['${currentDateData.year}${currentDateData.month}'][currentDateData.day.toString()];
  }

  /************ Http Request *************/
  /*
  * fetchDayReservations (depend on date - month and year)
  * get day reservation of a month + save data in allDayReservation
  */
  Future<dynamic> fetchDayReservations(DateTime date) {
    isLoadingData = true;
    notifyListeners();
    String month = date.month < 10 ? '0${date.month}' : date.month;
    // request
    return http.get(
      '${apiUrl}/api/restaurant/reservation_count/${date.year}${month}.do',
      headers: headers
    )
    // handle request response
    .then((http.Response response) {
      bool hasError = true;
      String message = 'Something went wrong';
      // success
      if(response.body != null && response.body != '') {
        final List<dynamic> responseData = json.decode(response.body);
        final _today = DateTime.now();
        final _prevYear = DateTime(_today.year - 1, _today.month, 1);
        final _nextYear = DateTime(_today.year + 1, _today.month, 1);
        hasError = false;
        message = 'Get data success';
        //save data
        // save data in day reservation if it's less than a year different from today
        if(date.isAfter(_prevYear) && date.isBefore(_nextYear)) {
          responseData.map((reservation) => addToDayReservations(reservation)).toList();
          _dayReservations['${date.year}${date.month}']['0'] =  true; // already got data for the month
        }
      } else { // error
        message = 'empty answer';
      }
      isLoadingData = false;
      notifyListeners();
      return {'message': message, 'success': !hasError};
    });
  }

  /************ Methods *************/
  /*
  * Add DayReservation
  *  Add a DayReservation to the list 
  */
  void addToDayReservations(Map<String, dynamic> reservation) {
     final DateTime tmpDate = DateTime.parse(reservation['reservationDate']);
     final DayReservation dayReservation = DayReservation(
      reservationDate: DateTime.parse(reservation['reservationDate']),
      dawn: reservation['dawn'],
      lunch: reservation['lunch'],
      dinner: reservation['dinner'],
      waiting: reservation['waiting'],
      guests: reservation['guests'],
      name: reservation['name']
    );
    if(_dayReservations['${tmpDate.year}${tmpDate.month}'] == null) {
      _dayReservations['${tmpDate.year}${tmpDate.month}'] = Map();
    } else {
      if (_dayReservations['${tmpDate.year}${tmpDate.month}'][tmpDate.day.toString()] == null)
        _dayReservations['${tmpDate.year}${tmpDate.month}'][tmpDate.day.toString()] = dayReservation;
    }
  }
}
