/*
* User Scoped Model
* Dependensies: User model - Connected Data Scoped Model
* Methods : 
* - get user, get userSubject
* - login, findId, findPassword, logout
*/

import 'dart:convert'; // convert/decode json
import 'dart:async';
import 'package:http/http.dart' as http; // http request
import 'package:shared_preferences/shared_preferences.dart'; // cookies
import 'package:rxdart/rxdart.dart';

import '../models/user.dart';
import './connected_data.dart';

class UserModel extends ConnectedDataModel {
  // Timer _authTimer; // token timer
  User _authenticatedUser;
  PublishSubject<bool> _userSubject = PublishSubject();

  /************ Getters *************/
  /*
  * GET User
  */
  User get user {
    return _authenticatedUser;
  }

  /*
  * GET UserSubject
  */
  PublishSubject<bool> get userSubject {
    return _userSubject;
  }

  /************ Http Request *************/
  /*
  * Login
  * login + save the user + create cookie
  */
  Future<Map<String, dynamic>> authenticate(String phone, String loginId, String password, String pushId) async {
    final Map<String, dynamic> authData = {
      'storePhoneNo': phone,
      'loginId': loginId,
      'loginPassword': password,
      'os': 'I',
      'pushId': pushId
    };

    //Request
    http.Response response;
    response = await http.post(
      '${apiUrl}//api/account/signin.do',
      body: json.encode(authData),
      headers: headers
    );
    print(json.decode(response.body));

    //Handle request response
    final Map<String, dynamic> responseData = json.decode(response.body);
    bool hasError = true;
    String message = 'Something went wrong';
    // Success
    if (responseData['success'] == null || responseData['success']) {
      hasError = false;
      message = 'Authentification succeeded';
      // save user
      _authenticatedUser = User(
        loginId: loginId,
        phone: phone,
        password: password,
        token: response.headers['x-dm-token'],
        restaurantName: responseData['restaurantName'],
        storeName: responseData['storeName'],
        planCode: responseData['store']['planCode'],
        hasSwitchStore: responseData['hasSwitchStore']
      );
      // headers
      headers['X-DM-TOKEN'] = response.headers['x-dm-token'];
      headers['User-Agent'] = 'iOS';
      // baseDate
      baseDateData = responseData['store']['baseDateViewType'];
      // setAuthTimeOut(int.parse(responseData['expiresIn']));
      _userSubject.add(true);
      // final DateTime now = DateTime.now();
      // final DateTime expiryTime = now.add(Duration(seconds: int.parse(responseData['expiresIn'])));
      // cookies 
      final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
      prefs.setString('token', response.headers['x-dm-token']);
      prefs.setString('phone', phone);
      prefs.setString('loginId', loginId);
      prefs.setString('password', password);
      prefs.setString('restaurantName', responseData['restaurantName']);
      prefs.setString('storeName', responseData['storeName']);
      prefs.setString('planCode', responseData['store']['planCode']);
      prefs.setBool('hasSwitchStore', responseData['hasSwitchStore']);
      prefs.setString('baseDate', responseData['store']['baseDateViewType']);
      // prefs.setString('expiryTime', expiryTime.toIso8601String());
    } else { //error
      message = responseData['message'];
    } 
    isLoadingData = false;
    notifyListeners();
    return {'message': message, 'success': !hasError};
  }



  /*
  * Find User Id
  */
  Future<Map<String, dynamic>> findId(String storeName, String phone) async {
    //request 
    http.Response response;
    response = await http.get(
      '${apiUrl}//api/account/find/id.do?name=${storeName}&phone=${phone}',
      headers: headers
    );
    
    //handle request response
    final Map<String, dynamic> responseData = json.decode(response.body);
    bool hasError = true;
    String message = 'Something went wrong';
    // success
    if (responseData['success'] == null || responseData['success']) {
      hasError = false;
      message = 'Id found succeeded';
    } else { //error
      message = '정학한 식당 이름 및 전화번호를 입력하여 주세요.';
    } 
    isLoadingData = false;
    notifyListeners();
    return {'message': message, 'success': !hasError};
  }



  /*
  * Find user password
  */
  Future<Map<String, dynamic>> findPassword(String email) async {
    // request
    http.Response response;
    response = await http.get(
      '${apiUrl}//api/account/find/password.do?email=${email}',
      headers: headers
    );
    
    // handle request response
    final Map<String, dynamic> responseData = json.decode(response.body);
    bool hasError = true;
    String message = 'Something went wrong';
    // success 
    if (responseData['success'] == null || responseData['success']) {
      hasError = false;
      message = 'Password found succeeded';
    } else { // error
      message = '가입하신 이메일에서 임시 비밀번호를 확인하세요.';
    } 
    isLoadingData = false;
    notifyListeners();
    return {'message': message, 'success': !hasError};
  }



  /*
  * Logout
  * logout + remove cookies - reset user
  */
  void logout() async {
    _authenticatedUser = null;
    // _authTimer.cancel();
    _userSubject.add(false);
    final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
    prefs.clear(); // clear all cookie data
    /* prefs.remove('token');
    prefs.remove('phone');
    prefs.remove('loginId');
    prefs.remove('password');
    prefs.remove('restaurantName');
    prefs.remove('storeName');
    prefs.remove('planCode');
    prefs.remove('hasSwitchStore');
    prefs.remove('baseDate');
    headers.remove('X-DM-TOKEN'); */
  }

  /*
  * Authentification time
  * logout when token expired
  */
  /* void setAuthTimeOut(int time) {
    _authTimer = Timer(Duration(seconds: time), () {
      logout();
    });
  } */



  /*
  * Auto Login
  * check if there is a token in cookies => login + create user
  */
  void autoAuthenticate() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance(); //async function
    final String token = prefs.getString('token');
    // final String expiryTimeString = prefs.getString('expiryTime');
    if (token != null) {
      //  DateTime now = DateTime.now();
      // /final parsedExpiryTime = DateTime.parse(expiryTimeString);
      /* if(parsedExpiryTime.isBefore(now)) {
        _authenticatedUser = null;
        notifyListeners();
        return;
      } */
      // Cookies
      final String loginId = prefs.getString('loginId');
      final String phone = prefs.getString('phone');
      final String password = prefs.getString('password');
      final String restaurantName = prefs.getString('restaurantName');
      final String storeName = prefs.getString('storeName');
      final String planCode = prefs.getString('planCode');
      final bool hasSwitchStore = prefs.getBool('hasSwitchStore');
      // Base Date
      baseDateData = prefs.getString('baseDate');
      // Header
      headers['X-DM-TOKEN'] = token;
      headers['User-Agent'] = 'iOS';
      // final tokenLifespan = parsedExpiryTime.difference(now).inSeconds;
      // setAuthTimeOut(tokenLifespan);
      _userSubject.add(true);
      // Create User
      _authenticatedUser = User(
        loginId: loginId,
        phone: phone,
        password: password,
        token: token,
        restaurantName: restaurantName,
        storeName: storeName,
        planCode: planCode,
        hasSwitchStore: hasSwitchStore
      );
      notifyListeners();
    }
  }
}